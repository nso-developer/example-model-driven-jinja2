# -*- mode: python; python-indent: 4 -*-
import ncs
from ncs.application import Service
from . import template

class ServiceCallbacks(Service):
    built_in_names = ['modified','directly-modified']


    def get_list(self, list_node):
        vars = []
        # Iterate over instances in the list     
        for elem in list_node:
            # Check for singleton lists.
            children = [ x for x in dir(elem) if not x.startswith('__')]
            if children == ['item']:
                vars.append(elem.item)
                continue

            rec = {}
            vars.append(rec)
            # Iterate over the schema of the instance
            for child in elem:
                name = child.split(':')[1]
                child_node = elem[child]
                if isinstance(child_node, ncs.maagic.List):
                    rec[name] = self.get_list(child_node)
                    continue
                rec[name] = str(child_node)
        return vars

    def get_vars(self, service):
        vars = {}
        for child in service:
            name = child.split(':')[1]

            child_node = service[child]

            # Skip known built-ins
            if name in self.built_in_names:
                continue

            # Skip types that we do not currently handle
            if isinstance(child_node, ncs.maagic.Container) or isinstance(child_node, ncs.maagic.Action) \
               or isinstance(child_node, ncs.maagic.LeafList):
                continue

            # Lists are special
            if isinstance(child_node, ncs.maagic.List):
                vars[name] = self.get_list(child_node)
                continue
            vars[name] = str(child_node)
        return vars

    @Service.create
    def cb_create(self, tctx, root, service, proplist):
        self.log.info('Service create(service=', service._path, ')')
        vars = self.get_vars(service)
        self.log.info("Got vars: {}".format(vars))
        for device in service.device:
            device_node = root.devices.device[device]
            template.applyTemplate(self.log, 'example.j2', device_node, vars)

# ---------------------------------------------
# COMPONENT THREAD THAT WILL BE STARTED BY NCS.
# ---------------------------------------------
class Main(ncs.application.Application):
    def setup(self):
        self.log.info('Main RUNNING')
        self.register_service('example', ServiceCallbacks)

    def teardown(self):
        self.log.info('Main FINISHED')
