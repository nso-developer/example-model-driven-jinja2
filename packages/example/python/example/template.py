import traceback
from jinja2 import Environment, FileSystemLoader

jinjatemplatedir = 'python'

def processJinja(dir, file, vars):
    env = Environment(loader=FileSystemLoader(dir))
    template = env.get_template(file)
    t = template.render(vars)
    return t

def applyTemplate(log, file, device, vars):

    # Extract the file of our caller using some black python magic
    stack = traceback.extract_stack() 
    caller = stack[-2]
    callfile = caller[0]
    callbase = ''
    es = callfile.split('/')[1:]

    # The base will be right after packages-in-use
    count = -1
    for e in es:
        if count:
            callbase += '/' + e
            count -= 1
        if e == 'packages-in-use':
            count = 2

    # We put all templates in the webui directory to make them work nicely with packages redeploy
    templbase = callbase + '/' + jinjatemplatedir
    t = processJinja(templbase, file, vars)
    log.info('Processed template:\n>>>\n{}\n<<<'.format(t))


    ln = device.load_native_config
    inp = ln.get_input()
    inp.data = t
    inp.verbose.create()
    inp.mode = 'merge'
    out = ln.request(inp)
    log.info('Template output: {}'.format(out.info))
