# Example: Model Driven Jinja2

This repository contains a few examples of model driven development in NSO that were presented at Developer Days 2020.

There are a few smaller examples of the schema API and the main example shows how to integrate Jinja2 templates into NSO by reading the model in runtime.

## The main example

The main example [example](packages/example) shows how to use a jinja2 template to load data into NSO. First start the example by using the instructions below. It is helpful to `tail -f ncs-python-vm-example.log`.

A sample instance:
```
example e1
 device  [ ios0 ]
 ip_addr 192.168.1.1
 ip_mask 255.255.255.0
 routes r1
  network 192.168.2.0
  mask    255.255.255.0
  gws 192.168.1.2
  !
 !
!
```

Compare `commit-dry run` to [example.j2](packages/example/python/example.j2).

You can edit the file and do `make testenv-build PACKAGE_RELOAD="true"` to observe changes.

## Smaller examples

Both of these are expected to be executed outside of NSO directly with a python interpeter.

The first example [islist.py](extra-files/src/examples/islist.py) shows two different ways of testing if a node in the tree is a list node or not.

The second one [dumptree.py](extra-files/src/examples/dumptree.py) shows how to do a schema dump of a sub-tree in a running NSO.

## Starting the main example in docker

The example is building using [NSO in Docker](https://gitlab.com/nso-developer/nso-docker), so if you want to run the examples there you first need to download a build NSO in Docker. 

I tested this with NSO version 5.3 and  IOS XE NED version 6.42.1, both which are available free of charge via Cisco Devnet. See the [template instructions](README.nid-package.org) for more information on the build system.

1. Copy a Cisco IOS NED to `test-packages/`
     cp ncs-5.3-cisco-ios-6.42.1.tar.gz test-packages/

2. Build the system
```
make build
make testenv-start
make testenv-build
make testenv-start-netsim
```

3. You can then get an NSO cli using
```
make testenv-clic
```

or the shell using
```
make testenv-shell
```

Logfiles are in `/log`.