#!/usr/bin/env python3

import ncs.maagic as maagic
import ncs.maapi as maapi
import _ncs as _ncs

def cs2type(cs):
    if cs.is_action(): return 'action'
    if cs.is_case(): return 'case'
    if cs.is_leaf(): return 'leaf'
    if cs.is_leaf_list(): return 'leaf-list'
    if cs.is_list(): return 'list'
    if cs.is_container(): return 'container'

def gettype(cs):
    if not(cs.is_leaf() or cs.is_leaf_list()):
        return ''
    if cs.is_empty_leaf():
        return 'empty'
    t = cs.info().shallow_type()
    if t == _ncs.C_BINARY: return 'binary'
    if t == _ncs.C_BOOL: return 'bool'
    if t == _ncs.C_DOUBLE: return 'double'
    if t == _ncs.C_BUF: return 'buf'
    if t == _ncs.C_INT8: return 'int8'
    if t == _ncs.C_INT16: return 'int16'
    if t == _ncs.C_INT32: return 'int32'
    if t == _ncs.C_INT64: return 'int64'
    if t == _ncs.C_UINT8: return 'uint8'
    if t == _ncs.C_UINT16: return 'uint16'
    if t == _ncs.C_UINT32: return 'uint32'
    if t == _ncs.C_UINT64: return 'uint64'
    if t == _ncs.C_UNION: return 'union'
    if t == _ncs.C_ENUM_VALUE: return 'enum'
    if t == _ncs.C_IPV4: return 'ipv4'
    if t == _ncs.C_IPV4PREFIX: return 'ipv4-prefix'
    if t == _ncs.C_IPV4_AND_PLEN: return 'ipv4-prefix-and-length'
    if t == _ncs.C_IPV4: return 'ipv6'
    if t == _ncs.C_IPV4PREFIX: return 'ipv6-prefix'
    if t == _ncs.C_IPV4_AND_PLEN: return 'ipv6-prefix-and-length'
    return 'UNKNOWN ({})'.format(t)


def dumpNode(cs, recursive=True, depth = 0):
    cstype = cs2type(cs)
    name = _ncs.hash2str(cs.tag())
    tval = gettype(cs)
    print('{}{} {} {}'.format('  '*depth, cstype, name, tval))
    c = cs.children()
    while c:
        dumpNode(c, depth=depth+1)
        c = c.next()


path = '/devices/global-settings/commit-queue'

if __name__ == '__main__':
    with maapi.single_read_trans('dumptree', 'system') as t:
        n = maagic.get_node(t, path)    
        print('Dumping {}::\n'.format(n._path))
        dumpNode(n._cs_node)
