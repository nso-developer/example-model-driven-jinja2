#!/usr/bin/env python3
import ncs.maapi as maapi
import ncs.maagic as maagic

def is_list(path):
    with maapi.single_read_trans('islist', 'system') as t:
        n = maagic.get_node(t, path)    
        return n._cs_node.is_list()

def is_list_maagic(path):
    with maapi.single_read_trans('islist',
         'system') as t:
        n = maagic.get_node(t, path)
        return isinstance(n, maagic.List)

def children(path):
    with maapi.single_read_trans('islist','system') as t:
        n = maagic.get_node(t, path)
        for child in n:
            (prefix, name) = child.split(':')
            cn = n[name]
            print('Child: {}, Type: {}'.format(name, type(cn)))

if __name__ == '__main__':
    print('Using is_list: /devices: {}, /devices/device: {}'.
        format(is_list('/devices'), is_list('/devices/device')))
    print('Using is_list_maagic: /devices: {}, /devices/device: {}'.
        format(is_list_maagic('/devices'), is_list_maagic('/devices/device')))
    print('Children of /devices')
    children('/devices')
